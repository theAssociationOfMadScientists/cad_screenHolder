/* Protective cover for Screen from Acer
	Aspire One craptop. I'll be receiving
	a driver board for this soon, and I'd
	like to use it as a camera monitor.
*/

//		**	PARAMETERS	**
inches_to_mm	= 25.4;
screen_height	= (5 + (1/8))*inches_to_mm;
screen_width	= (8 + (3/8))*inches_to_mm;
screen_depth	= 5;
screen_dims		= [screen_height,screen_width,screen_depth];
thickness		= 1;
fix				= .02;



//		**	 MODULES	**
module screen_actual(){
	cube(size=screen_dims);
}
module screen_case(){
	cube(size=screen_dims+[thickness*2,thickness*2,thickness]);
}
module gap_for_plug(){
	translate([34,0,0])cube([5,20,screen_depth+fix+thickness]);
	translate([15,0,screen_depth+thickness])rotate([0,3.4,0])cube([20,20,2]);
}


//		**	 ASSEMBLY	**
difference(){
	screen_case();
	translate([thickness,thickness,-fix]) screen_actual();
	translate ([ 0,40,0 ]) gap_for_plug();
}