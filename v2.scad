/* Protective cover for Screen from Acer
	Aspire One craptop. I'll be receiving
	a driver board for this soon, and I'd
	like to use it as a camera monitor.
*/

//		**	PARAMETERS	**
inches_to_mm	= 25.4;
screen_height	= (5 + (1/8))*inches_to_mm;
screen_width	= (8 + (3/8))*inches_to_mm;
screen_depth	= 5;
screen_dims		= [screen_height,screen_width,screen_depth];
thickness		= 1;
fix				= .02;
screw_holes		= [
	[ 12.5,0,screen_depth/2 ],
	[ screen_height-12.5, 0, screen_depth/2 ]
];
rail_size		= [3,40,2];
hinge_insideW	= 2;
hinge_insideH	= screen_depth;

//		**	 MODULES	**
module screen_actual(){
	cube(size=screen_dims);
}
module right_rim(){
	cube(size=([screen_height,screen_width/2, screen_depth]+[thickness*2, thickness*2,thickness]));
	
}
module gap_for_plug(){
	translate([34,40,0])cube([5,20,screen_depth+fix+thickness]);
	translate([15,40,screen_depth+thickness])rotate([0,3.4,0])cube([20,20,2]);
}
module screw_hole(){
	rotate([ -90,0,0])
		cylinder(d=1.5,
			h=screen_depth,
			center=true,
			$fs=.5);
	
}
module rail_outer(){
	difference(){
		cube(rail_size+[thickness,thickness,thickness]);
		translate([ thickness/2,thickness+fix*2,-fix ])
			cube(rail_size-[fix,fix,fix]);
	}
}
module rail_inner(){
	cube(rail_size);
}
module latch(){
	rotate([5,0,0])
	difference(){
		cube([ screen_depth,screen_depth*2,thickness ]);
		translate([ screen_depth/4,
			screen_depth,
			-fix])
			cube([ rail_size[2]+thickness/2,
				rail_size[0]+thickness,
				thickness+fix*2 ]);
	}
}
module hinge_inner(){
	difference(){
		rotate([90,0,0])minkowski(){
			linear_extrude(height=hinge_insideW)
				polygon(points=[
					[-screen_depth,0],
					[0,hinge_insideH],
					[screen_depth,0]]);
			cylinder(r=1,h=.01,$fs=.5);
		}
		translate([ 0,0,hinge_insideH*.85 ])screw_hole();
	}
}
module left_rim(){
	mirror([0,1,0]) difference(){
		right_rim();
		translate([thickness,thickness,-fix])screen_actual();
		for (i=screw_holes){
			translate(i)screw_hole();
		}
	}
}
//		**	 ASSEMBLY	**

// this union() has the right-side shape
translate([0,1,0])
union(){
	difference(){
		right_rim();
		translate([thickness,thickness,-fix])screen_actual();
		gap_for_plug();
		for (i=screw_holes){
			translate(i)screw_hole();
		}
	}
	translate([screen_height/5,
		screen_width/2-rail_size[1]/1.5,
		screen_depth+thickness]) 
			rail_outer();
	translate([(screen_height/5)*4,
		screen_width/2-rail_size[1]/1.5,
		screen_depth+thickness]) 
			rail_outer();
	translate([ screen_height/2,
		screen_width/2+thickness*2,
		screen_depth+thickness]) hinge_inner();
}
// this one, the left side.
translate([0,-1,0])
union(){
	left_rim();
	translate([ screen_height/2,
		-screen_width/2,
		screen_depth+thickness/2]) hinge_inner();
	translate([screen_height/5,
			-screen_width/2-rail_size[1]/1.5,
			screen_depth+thickness-fix]) 
		rail_inner();
	translate([(screen_height/5)*4,
			-screen_width/2-rail_size[1]/1.5,
			screen_depth+thickness-fix])
		rail_inner();
	
}