camera_mountSize	=	[17,17,1];
support_d			=	10;
support_r			=	support_d/2;
thickness			=	1;
socket_depth		=	10;
fix = 0.01;
module camera_mount(){
	cube (camera_mountSize,center=true);
}
module socket(){
	difference(){
		cylinder(r=support_r+thickness, 
			h=socket_depth+fix);
		translate([0,0,thickness])
			cylinder(r=support_r, 
				h=socket_depth,$fs=.01);
	}
}
module magnet(){
	translate([0,0,-thickness/2-fix])
		cylinder(h=2,d=8);
}
difference(){
	union(){
		camera_mount();
		socket();
	}
	magnet();
}