thickness		= 1;
hinge_insideW	= 4;
hinge_insideH	= 5;
pinhole			= 1;
hinge_outsideW	= hinge_insideW+thickness*3;
hinge_outsideH	= hinge_insideH;
2d23d			= [90,0,0];
support_d		= 10;
support_r		= support_d/2;
support_height	= 50;
fix				= .01;
module hinge(){
	rotate(2d23d)	difference(){
		minkowski(){
				linear_extrude(height=hinge_outsideW)
				polygon(points=[
					[-hinge_outsideW/2,0],
					[0,hinge_outsideH],
					[hinge_outsideW/2,0]]);
			cylinder(r=1,h=.001,$fs=.5);
		}
	translate([0,hinge_outsideH-pinhole,-.02])
			cylinder(r=pinhole,h=hinge_outsideW+.05,$fs=.5);
	translate([ -hinge_outsideW/2-1,pinhole-thickness/2,hinge_insideW/2-.5])
		cube([hinge_outsideW+2, hinge_outsideH+thickness,hinge_insideW]);
	}
}
module support(){
	cylinder(d=support_d,h=support_height,$fs=.01);
	translate([0,hinge_outsideW/2,support_height-hinge_outsideW-thickness])
		rotate(2d23d)
		linear_extrude(hinge_outsideW)
			polygon([
				[ 0,-thickness ],
				[ 0,(hinge_outsideW+thickness*2)*cos(30)],
				[ ((hinge_outsideW+support_r)*sin(30)+thickness), (hinge_outsideW+thickness*2)*cos(30)]
			]);
}
module magnet(){
	translate([0,0,-fix])
		cylinder(h=2,d=8+fix);
}
difference(){
	union(){
		support();
		translate([support_r,hinge_outsideW/2,support_height-hinge_outsideW/2-thickness/2])
			rotate([0,120,0])
				hinge();
	}
	rotate([0,0,10]) translate([0,support_r,0])cube([100,100,100]);
	rotate([0,0,-10])
		translate([0,-100-support_r,0])
			cube([100,100,100]);
	magnet();
}